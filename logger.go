package logger

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

// Log is the struct for log.
type Log struct {
	Stdout, Stderr              *logrus.Logger
	Outfile, Errfile, AccessLog string
	DebugFlag                   bool
}

// AccessLog is the struct for access.log.
type AccessLog struct {
	RemoteAddr string `json:"remote_addr"`
	RemoteUser string `json:"remote_user"`
	Time       string `json:"time"`
	Request    string `json:"request"`
	Status     int    `json:"status"`
	BodyBySent int    `json:"body_by_sent"`
	UserAgent  string `json:"user_agent"`
	LiveID     string `json:"live_id"`
	Referer    string `json:"referer"`
}

// NewLog is constructor of log.
func NewLog(out, err, acl string) *Log {
	l := new(Log)

	l.Outfile = out
	l.Errfile = err
	l.AccessLog = acl
	l.DebugFlag = true

	formatter := &logrus.TextFormatter{
		ForceColors: true,
		// DisableTimestamp: false,
		// FullTimestamp:    true,
	}
	// formatter := &logrus.JSONFormatter{}

	if out == "" {
		l.Stdout = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       os.Stdout,
		}
	}

	if err == "" {
		l.Stderr = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       os.Stderr,
		}
	}

	if out == "" && err == "" {
		return l
	}

	if os.Getenv("MODE") == "test" {
		l.Stdout = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       os.Stdout,
		}
		l.Stderr = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       os.Stderr,
		}
	} else {
		logOut, oerr := os.OpenFile(out, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		if oerr != nil {
			fmt.Printf("*** could not write stdout log: %v, %v", out, oerr)
			os.Exit(1)
		}

		logErr, eerr := os.OpenFile(err, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
		if eerr != nil {
			fmt.Printf("*** could not write stderr log: %v, %v", err, eerr)
			os.Exit(1)
		}

		l.Stdout = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       io.MultiWriter(os.Stdout, logOut),
		}
		l.Stderr = &logrus.Logger{
			Formatter: formatter,
			Level:     logrus.DebugLevel,
			Out:       io.MultiWriter(os.Stderr, logErr),
		}
	}

	return l
}

// Output write log to STDOUT.
func (l *Log) Output(calldepth int, str string) error {
	l.Info(str)
	return nil
}

// Prefix write the prefix with date.
func (l *Log) Prefix() string {
	t := time.Now()
	t.Local()
	return t.Format("2006/01/02 15:04:05") + ": "
}

// Info write the log with the info tag.
func (l *Log) Info(args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stdout.Info(args...)
}

// Debug write the log with the debug tag only on debug mode.
func (l *Log) Debug(args ...interface{}) {
	if !l.DebugFlag {
		return
	}

	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stdout.Debug(args...)
}

// Warn write the log with the warn tag.
func (l *Log) Warn(args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Warn(args...)
}

// Warning write the log with the warn tag.
func (l *Log) Warning(args ...interface{}) {
	l.Warn(args...)
}

// Error write the log with the error tag.
func (l *Log) Error(args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Error(args...)
}

// Fatal write the log with the fatal tag and exit on fail.
func (l *Log) Fatal(args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Fatal(args...)
}

// Debugf write the log with the debug tag only on debug mode with format.
func (l *Log) Debugf(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stdout.Debugf("%v"+format, args...)
}

// Infof write the log with the info tag with format.
func (l *Log) Infof(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stdout.Infof("%v"+format, args...)
}

// Warnf write the log with the warning tag with format.
func (l *Log) Warnf(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Warnf("%v"+format, args...)
}

// Warningf write the log with the warning tag with format.
func (l *Log) Warningf(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Warnf("%v"+format, args...)
}

// Errorf write the log with the error tag with format.
func (l *Log) Errorf(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Errorf("%v"+format, args...)
}

// Fatalf write the log with the fatal tag with format.
func (l *Log) Fatalf(format string, args ...interface{}) {
	args = append([]interface{}{interface{}(l.Prefix())}, args...)
	l.Stderr.Fatalf("%v"+format, args...)
}

// WriteAccessLog write the access log with JSON.
func (l *Log) WriteAccessLog(r *http.Request, status int, content, user string) {
	logPath := l.AccessLog

	if logPath == "" {
		return
	}

	ac, err := os.OpenFile(logPath, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0644)
	if err != nil {
		l.Error(fmt.Sprintf("*** Could not write the access log: %v", logPath))
	}
	defer ac.Close()

	t := time.Now().Local()
	ip := r.RemoteAddr
	ff := GetIPAdress(r)
	if ff != "" {
		ip = ff
	}

	al := &AccessLog{
		RemoteAddr: ip,
		Request:    r.RequestURI,
		Status:     status,
		BodyBySent: len([]byte(content)),
		Time:       t.Format("2006/01/02 15:04:05"),
		UserAgent:  r.UserAgent(),
		Referer:    r.Referer(),
		RemoteUser: user,
	}

	b, jerr := json.Marshal(al)
	if jerr != nil {
		l.Error(fmt.Sprintf("*** Could not parse to json: %v", jerr))
	}

	writer := bufio.NewWriter(ac)
	_, werr := writer.WriteString(string(b) + "\n")
	if werr != nil {
		l.Error(fmt.Sprintf("*** Could not write to access log: %v", werr))
	}

	writer.Flush()
}

// ipRange - a structure that holds the start and end of a range of ip addresses
type ipRange struct {
	start net.IP
	end   net.IP
}

// inRange - check to see if a given ip address is within a range given
func inRange(r ipRange, ipAddress net.IP) bool {
	// strcmp type byte comparison
	if bytes.Compare(ipAddress, r.start) >= 0 && bytes.Compare(ipAddress, r.end) < 0 {
		return true
	}
	return false
}

// privateRanges is array of the private address.
var privateRanges = []ipRange{
	ipRange{
		start: net.ParseIP("10.0.0.0"),
		end:   net.ParseIP("10.255.255.255"),
	},
	ipRange{
		start: net.ParseIP("100.64.0.0"),
		end:   net.ParseIP("100.127.255.255"),
	},
	ipRange{
		start: net.ParseIP("172.16.0.0"),
		end:   net.ParseIP("172.31.255.255"),
	},
	ipRange{
		start: net.ParseIP("192.0.0.0"),
		end:   net.ParseIP("192.0.0.255"),
	},
	ipRange{
		start: net.ParseIP("192.168.0.0"),
		end:   net.ParseIP("192.168.255.255"),
	},
	ipRange{
		start: net.ParseIP("198.18.0.0"),
		end:   net.ParseIP("198.19.255.255"),
	},
}

// isPrivateSubnet - check to see if this ip is in a private subnet
func isPrivateSubnet(ipAddress net.IP) bool {
	// my use case is only concerned with ipv4 atm
	if ipCheck := ipAddress.To4(); ipCheck != nil {
		// iterate over all our ranges
		for _, r := range privateRanges {
			// check if this ip is in a private range
			if inRange(r, ipAddress) {
				return true
			}
		}
	}
	return false
}

// GetIPAdress return the IP address from HTTP request.
func GetIPAdress(r *http.Request) string {
	ip := r.RemoteAddr
	var hip string

	for _, h := range []string{"X-Forwarded-For", "X-Real-Ip"} {
		addresses := strings.Split(r.Header.Get(h), ",")
		// march from right to left until we get a public address
		// that will be the address right before our proxy.
		for i := len(addresses) - 1; i >= 0; i-- {
			tmp := strings.TrimSpace(addresses[i])
			// header can contain spaces too, strip those out.
			realIP := net.ParseIP(ip)
			// if !realIP.IsGlobalUnicast() || isPrivateSubnet(realIP) {
			if !realIP.IsGlobalUnicast() {
				// bad address, go to next
				hip = tmp
				continue
			}
		}
	}

	if hip != "" {
		return hip
	}

	return ip
}
