# golang-logger

golangのログモジュールです。

## Usage

logger := NewLog(
    "/tmp/stdout.log", # 標準のログの書き出し先のパス
    "/tmp/stderr.log", # エラーログの書き出し先のパス
    "/tmp/access.log", # アクセスログの書き出し先のパス
    true,              # デバッグモードかどうか
)

### Info(...interface{})

logger.Info("hoge") // -> STDOUT

```
INFO[0090] 2019/02/14 15:48:41: hoge
```

### Error(...interface{})

logger.Error("hoge") // -> STDERR

```
FATA[0090] 2019/02/14 15:48:41: hoge
```

### Warn(...interface{})

logger.Warning("hoge") // -> STDERR

```
WARN[0090] 2019/02/14 15:48:41: hoge
```

### Debug(...interface{})

logger.Debug("hoge") // -> STDOUT

```
DEBU[0090] 2019/02/14 15:48:41: hoge
```

### WriteAccessLog(r *http.Request, status int, content, user string)

logger.WriteAccessLog(r, status, content, user) // -> ACCESSLOG

```
{"remote_addr":"67.205.129.52","remote_user":"","time":"2018/06/13 12:25:57","request":"/foo/bar","status":200,"body_by_sent":2,"user_agent":"curl/7.52.1","referer":""}
```
